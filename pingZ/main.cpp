#include <Winsock2.h>
#include <ws2tcpip.h>
#include <iphlpapi.h>
#include <windows.h>
#include <iostream>
#include <Icmpapi.h>

#pragma comment(lib, "iphlpapi.lib")
#pragma comment(lib, "Ws2_32.lib")


void main()
{
	HANDLE hIcmpFile;
	//PICMP_ECHO_REPLY *reply;
	DWORD dwRetVal;

	LPVOID ReplyBuffer;
	char SendData[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	ReplyBuffer = (VOID*)malloc(sizeof(ICMP_ECHO_REPLY) + sizeof(SendData));


	if ((hIcmpFile = IcmpCreateFile()) == INVALID_HANDLE_VALUE)
	{
		printf("\tUnable to open file.\n");
	}

	char *IPtable[1];
	unsigned long ip;

	IPtable[0] = ("8.8.8.8");

	if (!(ip = inet_addr(IPtable[0])))
		printf("conversion failed\n");
	std::cout << "\n" << "ip: " << ip << std::endl;

	if ((dwRetVal = IcmpSendEcho2(hIcmpFile, NULL, NULL, NULL, ip, SendData, sizeof(SendData), NULL, ReplyBuffer, 8 * sizeof(ReplyBuffer) + sizeof(ICMP_ECHO_REPLY), 1000)) != 0)

	{
		PICMP_ECHO_REPLY pEchoReply = (PICMP_ECHO_REPLY)ReplyBuffer;
		printf("\tReceived %ld messages.\n", dwRetVal);
		printf("\tMessage: %s\n\n", pEchoReply->Data);

		std::cout << "IPaddress: " << pEchoReply->Address << std::endl;
		std::cout << "Status: " << pEchoReply->Status << std::endl;
		std::cout << "RoundTripTime: " << pEchoReply->RoundTripTime << std::endl;
		std::cout << "DataSize: " << pEchoReply->DataSize << std::endl;
		std::cout << "Data: " << pEchoReply->Data << std::endl;

	}
	else
	{
		printf("\tCall to IcmpSendEcho() failed.\n");
		printf("\tError: %ld\n", GetLastError());
	}

	CloseHandle(hIcmpFile);            //clean stuff up

	return;
}
